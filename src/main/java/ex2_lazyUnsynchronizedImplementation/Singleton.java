package ex2_lazyUnsynchronizedImplementation;

// не работает в многопоточной среде.
public class Singleton {
    private static Singleton instanse;

    private Singleton() {
    }

    public static Singleton getInstance() {
        if (instanse == null) {
            instanse = new Singleton();
        }
        return instanse;
    }
}
