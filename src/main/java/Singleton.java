public class Singleton {
    private static final Singleton INSTANCE;

    static {
        Singleton inst;
        try {
            inst = new Singleton("name1");
        } catch (Exception exception) {
            exception.printStackTrace();
            inst = new Singleton("name2");
        }
        INSTANCE = inst;
    }

    private Singleton(String name) {
        if (name == null) {
            throw new RuntimeException();
        }
    }
}
