package ex5_enumSingleton;
//недостаток: нет насследования.Лучше не использовать

public enum Singleton {
    INSTANCE;

    public void someMethod() {
    }

    public void anotherMethod() {
    }
}

